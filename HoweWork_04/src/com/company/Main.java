package com.company;
import java.util.Scanner;

public class Main {

//функция вывода четных чисел из массива
    public static void ifEven (int []array) {
        System.out.print("Четные числа в этом массиве:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.print(" " + array[i]);
            }
        }
        System.out.println(" ");
    }

//функция, возвращающая сумму чисел в каком-либо интервале массива
    public static int Sum (int []array, int from, int to) {
        int summa = 0;
        if (from > -1 && from < array.length && to < array.length && to > -1) {
            for (int i = from+1; i < to; i++) {
                summa = summa + array[i];
            }
        return summa;
        }
        else return -1;
    }

//функция toInt
    public static void toInt (int []array) {
        long n = 0;
        long m = 1;
        for (int i = array.length-1; i > -1; i--) {
            n = n + array [i]*m;
            m= m*10;
        }
        System.out.println("toInt - " + n);
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

//инициализируем и заполняем массив
        System.out.println("Введите длину массива: ");
        int size = input.nextInt();
        int []array = new int[size];
        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < size; i++) {
            array[i] = input.nextInt();
        }
        System.out.println("Введите левую границу интервала: ");
        int from = input.nextInt();
        System.out.println("Введите правую границу интервала: ");
        int to = input.nextInt();

// Вызываем функции
        int a = Sum(array,from,to);
        System.out.println("Сумма элементов в заданном интервале: " + a);

        ifEven(array);

        toInt(array);
    }
}