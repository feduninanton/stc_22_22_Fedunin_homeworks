package com.company;

public class Cars {
        private String RegistrationNumber;
        private String Model;
        private  String Color;
        private Integer Mileage;
        private Integer Price;

    public Cars(String registrationNumber, String model, String color, Integer mileage, Integer price) {
        this.RegistrationNumber = registrationNumber;
        this.Model = model;
        this.Color = color;
        this.Mileage = mileage;
        this.Price = price;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public String getModel() {
        return Model;
    }

    public String getColor() {
        return Color;
    }

    public Integer getMileage() {
        return Mileage;
    }

    public Integer getPrice() {
        return Price;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "RegistrationNumber='" + RegistrationNumber + '\'' +
                ", Model='" + Model + '\'' +
                ", Color='" + Color + '\'' +
                ", Mileage=" + Mileage +
                ", Price=" + Price +
                '}';
    }
}


