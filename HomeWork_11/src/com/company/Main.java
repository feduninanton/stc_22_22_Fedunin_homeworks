package com.company;

public class Main {

    public static void main(String[] args) {
	CarsRepository CarsRepository = new CarsRepositoryFileBasedImpl("cars.txt");
        System.out.println(CarsRepository.CamryMiddlePrice());  // Средняя цена на Камри
        System.out.println(" ");
        Cars LowCostCar = CarsRepository.ColorMinPrice();
        System.out.println(LowCostCar.getColor());             // Цвет самой дешевой машины
        System.out.println(" ");
        System.out.println(CarsRepository.AmountUniqCars());   // Количество уник. машин в ценовом диапозоне
    }
}
