package ru.inno.finalattestation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalattestation.models.User;


import java.util.List;


public interface UserRepository extends JpaRepository<User,Long> {
    List<User> findAllByStateNot(User.State state);

}