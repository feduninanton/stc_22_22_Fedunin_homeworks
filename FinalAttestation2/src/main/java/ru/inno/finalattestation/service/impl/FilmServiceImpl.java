package ru.inno.finalattestation.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalattestation.dto.FilmForm;
import ru.inno.finalattestation.models.Film;
import ru.inno.finalattestation.repository.FilmRepository;
import ru.inno.finalattestation.service.FilmService;

import java.util.List;

@Service
@RequiredArgsConstructor

public class FilmServiceImpl implements FilmService {
    private final FilmRepository filmRepository;

    @Override
    public void deleteFilm(Long filmId) {
        Film filmForDelete = filmRepository.findById(filmId).orElseThrow();
        filmRepository.delete(filmForDelete);
    }

    @Override
    public List<Film> getAllFilms() {
        return filmRepository.findAll();
    }

    @Override
    public void addFilm(FilmForm film) {
       Film newFilm = Film.builder()
                .title(film.getTitle())
                .description(film.getDescription())
                .duration(film.getDuration())
                .build();
        filmRepository.save(newFilm);
    }

    @Override
    public Film getFilm(long filmId) {
        return filmRepository.findById(filmId).orElseThrow();
    }


    @Override
    public void updateFilm(Long filmId, FilmForm updateData) {
        Film filmForUpdate = filmRepository.findById(filmId).orElseThrow();
        filmForUpdate.setTitle(updateData.getTitle());
        filmForUpdate.setDescription(updateData.getDescription());
        filmRepository.save(filmForUpdate);
    }
}

