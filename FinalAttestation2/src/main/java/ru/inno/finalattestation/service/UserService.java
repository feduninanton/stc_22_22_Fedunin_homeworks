package ru.inno.finalattestation.service;

import ru.inno.finalattestation.dto.UserForm;
import ru.inno.finalattestation.models.User;


import java.util.List;


public interface UserService {
    List<User> getAllUsers();

    void addUser(UserForm user);

      User getUser(long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}