package ru.inno.finalattestation.service.impl;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalattestation.dto.UserForm;
import ru.inno.finalattestation.models.User;
import ru.inno.finalattestation.repository.UserRepository;
import ru.inno.finalattestation.service.UserService;


import java.util.List;

@Service
@RequiredArgsConstructor

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

     @Override
     public List<User> getAllUsers() {
        return userRepository.findAllByStateNot(User.State.DELETED);
  }

    @Override
    public void addUser(UserForm user) {
        User newUser = User.builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .state(User.State.CONFIRMED)
                .build();
        userRepository.save(newUser);
    }

      @Override
     public User getUser(long id) {
        return userRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateUser(Long userId, UserForm updateData) {
        User userForUpdate = userRepository.findById(userId).orElseThrow();
        userForUpdate.setFirstName(updateData.getFirstName());
        userForUpdate.setLastName(updateData.getLastName());
        userRepository.save(userForUpdate);
    }

    @Override
    public void deleteUser(Long userId) {
        User userForDelete = userRepository.findById(userId).orElseThrow();
        userForDelete.setState(User.State.DELETED);
        userRepository.save(userForDelete);
    }
}