package ru.inno.finalattestation.service;

import ru.inno.finalattestation.dto.TVserialForm;
import ru.inno.finalattestation.models.TVserial;

import java.util.List;

public interface TVserialService {
    List<TVserial> getAllTVserials();

    void addTVserial(TVserialForm tvserial);

    void deleteTVserial(Long tvserialId);

    void updateTVserial(Long tvserialId, TVserialForm tvserial);

    TVserial getTVserial(long tvserialId);
}
