package ru.inno.finalattestation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.finalattestation.dto.TVserialForm;
import ru.inno.finalattestation.service.TVserialService;

@Controller
@RequiredArgsConstructor

public class TVserialController {

    private final TVserialService tvserialService;

    @GetMapping("/tvserials")
    public String GetTVserialPage( @RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("tvserials", tvserialService.getAllTVserials());
        return "tvserials_page";
    }

    @PostMapping("/tvserials")
    public String addTVserial(TVserialForm tvserial) {
        tvserialService.addTVserial(tvserial);
        return "redirect:/tvserials";
    }

    @GetMapping("/tvserials/{tvserial-id}")
    public String getTVserialPage(@PathVariable("tvserial-id")long tvserialId, Model model) {
        model.addAttribute("tvserial", tvserialService.getTVserial(tvserialId));
        return "tvserial_page";
    }

    @GetMapping("/tvserials/{tvserial-id}/delete")
    public String deleteTVserial(@PathVariable("tvserial-id") Long tvserialId){
        tvserialService.deleteTVserial(tvserialId);
        return "redirect:/tvserials";
    }

    @PostMapping("/tvserials/{tvserial-id}/update")
    public String updateTVserial(@PathVariable("tvserial-id") Long tvserialId, TVserialForm tvserial) {
        tvserialService.updateTVserial(tvserialId, tvserial);
        return "redirect:/tvserials";
    }
}
