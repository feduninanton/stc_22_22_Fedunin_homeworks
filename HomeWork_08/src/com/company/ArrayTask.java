package com.company;
      // функциональный интерфейс с методом без реализации
public interface ArrayTask {
      int resolve (int[] array, int from, int to);
}
