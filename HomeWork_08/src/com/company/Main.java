package com.company;

public class Main {
    public static void main(String[] args) {
   // Объявляем массив и переменные from и to
        int a = 1;
        int b = 5;
        int [] array1 ={3, 8, 34, 7, 40, 78, 1, 2, 60};
   // первое лямбда выражение
        ArrayTask task1;
        task1 = (array , from, to) -> {
            int summa = 0;
            for (int i=from; i<=to; i++){
              summa = summa + array[i];
            }
            System.out.println("сумма элементов массива в промежутке от "
                               + from + " до " + to + " равна " + summa);
            return summa;
        };
   // второе лямбда выражение
        ArrayTask task2;
        task2 = (array , from, to) -> {
            int summa = 0;
            int maxNum = array[from];
            for (int i=from; i<=to; i++){

               if (maxNum<array[i+1]) {
                   maxNum = array[i+1];
                }
            }
            while (maxNum>0) {
             summa = summa + maxNum % 10;
             maxNum = maxNum /10;
            }
            System.out.println("Сумма цифр самого большого числа: " + summa);
            return summa;
        };
   // вызываем лямбда выражения через класс ArraysTasksResolver
        ArraysTasksResolver.resolveTask(array1, task1, a, b );
        ArraysTasksResolver.resolveTask(array1, task2, a, b );
    }
}

