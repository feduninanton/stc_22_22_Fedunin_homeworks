package com.company;

public class Circle extends figure{

    public Circle (int x, int y, double smallRadius){
        this.x = x;
        this.y = y;
        this.smallRadius = smallRadius;
    }
    @Override
    public double getPerimetr() {
        double T = 2*3.14*smallRadius;
        System.out.println("Периметр круга = " + T);
        return T;
    }

    @Override
    public double getArea() {
        double O = 2*3.14*smallRadius*smallRadius;
        System.out.println("Площадь круга = " + O);
        return O;
    }


}
