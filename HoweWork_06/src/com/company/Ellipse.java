package com.company;

public class Ellipse extends Circle {
    public Ellipse(int x, int y, double smallRadius, double bigRadius) {
        super(x, y, smallRadius);
        this.bigRadius = bigRadius;
    }
    @Override
    public double getPerimetr() {
        double S = 2*3.14*(Math.sqrt((smallRadius*smallRadius + bigRadius*bigRadius)/2));
        System.out.println("Периметр эллипса = " + S);
        return S;

    }

    @Override
    public double getArea() {
        double L = 3.14*smallRadius*bigRadius;
        System.out.println("Площадь эллипса = " + L);
        return L ;
    }


}
