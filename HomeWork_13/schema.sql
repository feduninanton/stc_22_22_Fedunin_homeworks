create table cars(
id bigserial primary key,
model varchar,
color varchar,
registration_number varchar,
mileage integer
);