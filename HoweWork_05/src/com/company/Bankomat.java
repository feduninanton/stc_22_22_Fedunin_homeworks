package com.company;

import org.w3c.dom.ls.LSOutput;

public class Bankomat {
// поля класса
    static int moneyInStorage;   // Сумма оставшихся денег в банкомате
    int maxMoney;   // Максимальная сумма, разрешенная к выдаче
    int maxMoneyInStorage;   // Максимальный объем денег(сумма,которая может быть в банкомате)
    static int countOperation;   // Количество проведенных операций
// конструктор
    public Bankomat(int maxMoney, int maxMoneyInStorage) {
        this.maxMoney = maxMoney;
        this.maxMoneyInStorage = maxMoneyInStorage;
        moneyInStorage = maxMoneyInStorage;
    }
// описание методов
    public int cashOut(int money) {
        if (money <= this.maxMoney && moneyInStorage - money >= 0) {
            System.out.println("сумма выдана");
            moneyInStorage = moneyInStorage - money;
            countOperation++;
        } else if (money > this.maxMoney) {
            System.out.println("Превышен лимит снятия");
        } else if (moneyInStorage - money < 0) {
            System.out.println("В банкомате не хватает денег");
        }
        return money;
    }

    public int cashIn(int money) {
        if (money + moneyInStorage <= this.maxMoneyInStorage) {
            System.out.println("деньги внесены");
            moneyInStorage = moneyInStorage + money;
            countOperation++;
        }
        else System.out.println("превышен лимит банкомата");
        return money;
    }

    public static void menu () {

        System.out.println("нажмите 1 чтобы снять деньги");
        System.out.println("нажмите 2 чтобы внести деньги");
        System.out.println("нажмите 3 чтобы выйти");

    }


}
