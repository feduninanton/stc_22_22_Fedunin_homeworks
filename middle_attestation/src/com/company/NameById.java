package com.company;

import java.util.List;
import java.util.Scanner;

public class NameById {
    public static void getNameById() {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("Products.txt");
        int a;
        List<Products> NameById;
        System.out.println("Введите id продукта: ");
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        NameById = productsRepository.NameById(a);
        if (NameById.size() == 0) {
            System.out.println("продукта с таким Id нет в списке");
        } else {
            System.out.println("Продукт " + a + " это " + NameById);
        }
    }
}

