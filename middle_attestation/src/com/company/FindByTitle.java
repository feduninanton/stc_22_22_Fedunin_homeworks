package com.company;

import java.util.List;
import java.util.Scanner;

public class FindByTitle {
    public static void findAllByTitleLike() {
        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("Products.txt");
        String titleIn;
        String titleInLowCase;
        List<Products> findByTitle;
        System.out.println("Введите символы для поиска: ");
        Scanner input = new Scanner(System.in);
        titleIn = input.nextLine();
        titleInLowCase = titleIn.toLowerCase();
        findByTitle = productsRepository.findAllByTitleLike(titleInLowCase);
        if (findByTitle.size() == 0){
            System.out.println("Названий с такими символами нет в списке");
        }
        else {
            System.out.println("Результаты поиска: " + findByTitle);
        }
    }
}
