package com.company;

public class Products {
    private Integer id;
    private String name;
    private Double price;
    private Integer remains;

    public Products(Integer id, String name, Double price, Integer remains) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.remains = remains;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", remains=" + remains +
                '}';
    }
}
