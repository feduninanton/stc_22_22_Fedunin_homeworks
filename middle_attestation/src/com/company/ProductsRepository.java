package com.company;

import java.util.List;

public interface ProductsRepository {
    List<Products> NameById (int currentId);
    List<Products> findAllByTitleLike (String title);
}

