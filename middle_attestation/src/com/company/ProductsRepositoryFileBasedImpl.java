package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String FileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        FileName = fileName;
    }

    @Override
    public List<Products> NameById(int currentId) {
        List<Products> products = new ArrayList<>();

        try (FileReader fileReader = new FileReader(FileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
             String currentProduct = reader.readLine();
            while (currentProduct != null) {
                String[] parts = currentProduct.split("\\|");
                Integer id = Integer.parseInt(parts[0]);
                String name = parts[1];
                Double price = Double.parseDouble(parts[2]);
                Integer remains = Integer.parseInt(parts[3]);
                Products product = new Products(id, name, price, remains);
                if (id == currentId) {
                    products.add(product);
                }
                currentProduct = reader.readLine();
            }
        }
        catch (IOException e){
            throw new FileError (e);
        }
        return products;
    }

    @Override
    public List<Products> findAllByTitleLike(String title) {
        List<Products> products = new ArrayList<>();

        try (FileReader fileReader = new FileReader(FileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
             String currentProduct = reader.readLine();

            while (currentProduct != null) {
                String[] parts = currentProduct.split("\\|");
                Integer id = Integer.parseInt(parts[0]);
                String nameIn = parts[1];
                Double price = Double.parseDouble(parts[2]);
                Integer remains = Integer.parseInt(parts[3]);
                String name = nameIn.toLowerCase();
                Products product = new Products(id, name, price, remains);
                if (name.contains(title)) {
                    products.add(product);
                }
                currentProduct = reader.readLine();
            }
        }
        catch (IOException e){
            throw new FileError (e);
        }
        return products;
    }
}

