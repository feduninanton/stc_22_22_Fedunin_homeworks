package com.company;

public class FileError extends RuntimeException{
    public FileError (Exception e){
        super(e);
    }
}
