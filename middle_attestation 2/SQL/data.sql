insert into drivers (first_name, last_name, phone_number, experience, age, is_driver_license, driver_license_category,
                     rating)
values ('Ivan','Sorokin',89134567812,10,30,true,'B',4);
insert into drivers (first_name, last_name, phone_number, experience, age, is_driver_license, driver_license_category,
                     rating)
values ('Mike','Sanarov',89133781867,15,35,true,'B',5);
insert into drivers (first_name, last_name, phone_number, experience, age, is_driver_license, driver_license_category,
                     rating)
values ('Ostin','Powers',89459520056,5,25,true,'B',4);
insert into drivers (first_name, last_name, phone_number, experience, age, is_driver_license, driver_license_category,
                     rating)
values ('Artem','Nikishev',89521232233,20,40,true,'B',3);
insert into drivers (first_name, last_name, phone_number, experience, age, is_driver_license, driver_license_category,
                     rating)
values ('Vova','Sidorov',89459027711,30,50,true,'B',2);


insert into cars (model, color, registration_number, id_owners)
values ('camry', 'black', 'o666oo70', 1);
insert into cars (model, color, registration_number, id_owners)
values ('focus', 'red', 'x505xa70', 2);
insert into cars (model, color, registration_number, id_owners)
values ('lancer', 'white', 'o823po70', 3);
insert into cars (model, color, registration_number, id_owners)
values ('corolla', 'silver', 'a714ex70', 4);
insert into cars (model, color, registration_number, id_owners)
values ('logan', 'blue', 'y075ao70', 5);


insert into cruise (driver, car, cruise_date, cruise_duration)
values (1,'o666oo70','2022-11-20','02:30');
insert into cruise (driver, car, cruise_date, cruise_duration)
values (2,'o823po70','2022-11-21','03:45');
insert into cruise (driver, car, cruise_date, cruise_duration)
values (3,'o823po70','2022-11-22','01:00');
insert into cruise (driver, car, cruise_date, cruise_duration)
values (4,'o666oo70','2022-11-22','01:15');
insert into cruise (driver, car, cruise_date, cruise_duration)
values (5,'x505xa70','2022-11-19','02:00');
insert into cruise (driver, car, cruise_date, cruise_duration)
values (5,'y075ao70','2022-11-18','04:00');