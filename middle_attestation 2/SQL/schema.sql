create table drivers
( id bigserial primary key,
  first_name char(30) not null ,
  last_name char(30) not null ,
  phone_number bigint unique ,
  experience integer,
  age integer check (age >0 and age <=120),
  is_driver_license bool not null ,
  driver_license_category char(1) not null ,
  rating integer check (rating >=0 and rating <=5)
);

create table cars
( model char(30) not null ,
  color char(20) ,
  registration_number char(9) unique not null ,
  id_owners bigint,
  foreign key (id_owners) references drivers(id)
);

create table cruise
( driver bigint not null ,
  car char(30) not null ,
  cruise_date date,
  cruise_duration time(0),
  foreign key (driver) references drivers (id),
  foreign key (car) references cars (registration_number)
);