package com.company;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String str = "Hello Bye Hi Hello Day Fine Good Hello Bye Hello";
        String[] words = str.split(" ");
        HashMap <String, Integer> myHashMap = new HashMap<>();

        for (String string : words){
            if (myHashMap.keySet().contains(string)) {
                myHashMap.put(string, myHashMap.get(string)+1);
            }
            else {
                myHashMap.put(string,1);
            }
        }
               Map.Entry<String, Integer> maxValue = myHashMap.entrySet()
                .stream()
                .max(Comparator.comparing(Map.Entry::getValue))
                .orElse(null);

        // System.out.println(myHashMap);
        System.out.println(maxValue);
    }
}
