package ru.inno.homework_14.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.homework_14.models.Course;
import ru.inno.homework_14.models.Lesson;

import java.util.List;


public interface LessonRepository extends JpaRepository<Lesson,Long> {

List<Lesson> findAllByCoursesNotContains(Course course);

    List<Lesson> findAllByCoursesContains(Course course);
}