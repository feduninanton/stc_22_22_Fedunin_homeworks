package ru.inno.homework_14.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.homework_14.models.Course;


public interface CourseRepository extends JpaRepository<Course,Long>{

}






