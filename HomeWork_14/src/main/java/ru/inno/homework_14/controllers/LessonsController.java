package ru.inno.homework_14.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.homework_14.dto.LessonForm;
import ru.inno.homework_14.services. LessonService;


    @Controller
    @RequiredArgsConstructor

    public class LessonsController {
        private final  LessonService  lessonService;


        @GetMapping("/lessons")
        public String GetCoursePage( @RequestParam(value = "orderBy", required = false) String orderBy,
                                     @RequestParam(value = "dir", required = false) String direction, Model model) {
            model.addAttribute("lessons",  lessonService.getAllLessons());
            return "lessons_page";
        }

        @PostMapping("/lessons")
        public String addCourse(LessonForm Lesson) {
            lessonService.addLesson(Lesson);
            return "redirect:/lessons";
        }

        @GetMapping("/lessons/{lesson-id}")
        public String getLessonPage(@PathVariable("lesson-id")long id, Model model) {
            model.addAttribute("lesson", lessonService.getLesson(id));
            return "lesson_page";
        }

        @GetMapping("/lessons/{lesson-id}/delete")
        public String deleteLesson(@PathVariable("lesson-id") Long lessonId){
            lessonService.deleteLesson(lessonId);
            return "redirect:/lessons";
        }

        @PostMapping("/lessons/{lesson-id}/update")
        public String updateCourse(@PathVariable("lesson-id") Long lessonId, LessonForm lesson) {
            lessonService.updateLesson(lessonId, lesson);
            return "redirect:/lessons/" + lessonId;
        }
    }



