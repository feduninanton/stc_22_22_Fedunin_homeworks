package ru.inno.homework_14.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.homework_14.dto.CourseForm;
import ru.inno.homework_14.services.CourseService;

@Controller
@RequiredArgsConstructor

public class CoursesController {
     private final CourseService courseService;

    @GetMapping("/main")
    public String getMainPage(){
        return "main_page";
    }

    @GetMapping("/courses")
        public String GetCoursePage( @RequestParam(value = "orderBy", required = false) String orderBy,
        @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("courses", courseService.getAllCourses());
        return "courses_page";
    }

    @PostMapping("/courses")
        public String addCourse(CourseForm course) {
        courseService.addCourse(course);
        return "redirect:/courses";
    }

    @GetMapping("/courses/{course-id}")
        public String getCoursePage(@PathVariable("course-id")long courseId, Model model) {
        model.addAttribute("course", courseService.getCourse(courseId));
        model.addAttribute("lessonsNotInCourse", courseService.getLessonsNotInCourse(courseId));
        model.addAttribute("inCourseLessons", courseService.getLessonsInCourse(courseId));
        return "course_page";
    }

    @GetMapping("/courses/{course-id}/delete")
        public String deleteCourse(@PathVariable("course-id") Long courseId){
        courseService.deleteCourse(courseId);
        return "redirect:/courses";
    }

    @PostMapping("/courses/{course-id}/update")
        public String updateCourse(@PathVariable("course-id") Long courseId, CourseForm course) {
        courseService.updateCourse(courseId, course);
        return "redirect:/courses/" + courseId;
    }

    @PostMapping("/courses/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                    @RequestParam("lesson-id") Long lessonId) {
        courseService.addLessonToCourse(courseId, lessonId);
        return  "redirect:/courses/" + courseId;
    }
}

