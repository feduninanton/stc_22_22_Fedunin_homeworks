package ru.inno.homework_14.services;

import ru.inno.homework_14.dto.LessonForm;
import ru.inno.homework_14.models.Lesson;

import java.util.List;

public interface LessonService {

    List<Lesson> getAllLessons();

    void addLesson(LessonForm lesson);

    Lesson getLesson(long id);

    void updateLesson(Long lessonId, LessonForm lesson);

    void deleteLesson(Long lessonId);
}