package ru.inno.homework_14.services.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.homework_14.dto.LessonForm;
import ru.inno.homework_14.models.Lesson;
import ru.inno.homework_14.repository.LessonRepository;
import ru.inno.homework_14.services.LessonService;

import java.util.List;

@Service
@RequiredArgsConstructor

public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;

    @Override
    public List<Lesson> getAllLessons() {

        return lessonRepository.findAll();
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .build();
        lessonRepository.save(newLesson);
    }

    @Override
    public Lesson getLesson(long id) {

        return lessonRepository.findById(id).orElseThrow();
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson LessonForDelete  = lessonRepository.findById(lessonId).orElseThrow();
        lessonRepository.delete(LessonForDelete);
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateData ) {
        Lesson LessonForUpdate = lessonRepository.findById(lessonId).orElseThrow();
        LessonForUpdate.setName(updateData.getName());
        lessonRepository.save(LessonForUpdate);
    }
}
