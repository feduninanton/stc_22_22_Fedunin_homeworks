package ru.inno.homework_14.services.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.homework_14.dto.CourseForm;
import ru.inno.homework_14.models.Course;
import ru.inno.homework_14.models.Lesson;
import ru.inno.homework_14.repository.CourseRepository;
import ru.inno.homework_14.repository.LessonRepository;
import ru.inno.homework_14.services.CourseService;
import java.util.List;

@Service
@RequiredArgsConstructor

public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public void addCourse(CourseForm course) {
            Course newCourse = Course.builder()
                    .title(course.getTitle())
                    .description(course.getDescription())
                    .build();
            courseRepository.save(newCourse);

    }

    @Override
    public Course getCourse(long id) {

        return courseRepository.findById(id).orElseThrow();
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDelete  = courseRepository.findById(courseId).orElseThrow();
            courseRepository.delete(courseForDelete);
    }

    @Override
    public void updateCourse(Long courseId, CourseForm updateData ) {
        Course courseForUpdate = courseRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(updateData.getTitle());
        courseForUpdate.setDescription(updateData.getDescription());
        courseRepository.save(courseForUpdate);
    }

    @Override
    public void addLessonToCourse(Long courseId, Long lessonId) {
        Course course = courseRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonRepository.findById(lessonId).orElseThrow();

        lesson.getCourses().add(course);
                lessonRepository.save(lesson);
    }

    @Override
    public List<Lesson> getLessonsNotInCourse(long courseId) {
        Course course = courseRepository.findById(courseId).orElseThrow();
        List<Lesson> lessons = lessonRepository.findAllByCoursesNotContains(course);
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsInCourse(long courseId) {
        Course course = courseRepository.findById(courseId).orElseThrow();
        List<Lesson> lessons = lessonRepository.findAllByCoursesContains(course);
        return lessons;
    }

}
