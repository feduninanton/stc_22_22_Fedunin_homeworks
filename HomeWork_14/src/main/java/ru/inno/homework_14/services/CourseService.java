package ru.inno.homework_14.services;

import ru.inno.homework_14.dto.CourseForm;
import ru.inno.homework_14.models.Course;
import ru.inno.homework_14.models.Lesson;

import java.util.List;

public interface CourseService {
    List<Course> getAllCourses();

    void addCourse(CourseForm course);

    Course getCourse(long id);

    void deleteCourse(Long courseId);

    void updateCourse(Long courseId, CourseForm course);

    void addLessonToCourse(Long courseId, Long lessonId);

    List<Lesson> getLessonsNotInCourse(long courseId);

    List<Lesson> getLessonsInCourse(long courseId);
}
