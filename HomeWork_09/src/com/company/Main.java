package com.company;

public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();

        Iterator<String> documentsIterator = documents.iterator();

        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }

        return mergedDocument.toString();
    }

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");

        stringList.remove("Bye!");
        stringList.removeAt(3);


        List<Integer> integerList = new LinkedList<>();
        integerList.add(63);
        integerList.add(11);
        integerList.add(42);
        integerList.add(12);
        integerList.add(20);
        integerList.add(78);

        String documents = mergeDocuments(stringList);
       // mergeDocuments(integerList);

        System.out.println(documents);

    }
}