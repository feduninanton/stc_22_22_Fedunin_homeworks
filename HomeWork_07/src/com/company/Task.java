package com.company;

public interface Task {
    abstract void complete();
}
