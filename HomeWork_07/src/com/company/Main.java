package com.company;

public class Main {

    public static void main(String[] args) {
        Task t1 = new EvenNumbersPrintTask(1, 20);
        Task t2 = new OddNumbersPrintTask(5,30);
      completeAllTasks( new Task [] {t1,t2});
           }
    public static void completeAllTasks (Task[] tasks){
        for (Task t:tasks){
            t.complete();
        }
    }
}
