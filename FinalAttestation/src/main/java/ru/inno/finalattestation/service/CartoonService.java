package ru.inno.finalattestation.service;

import ru.inno.finalattestation.dto.CartoonForm;
import ru.inno.finalattestation.models.Cartoon;

import java.util.List;

public interface CartoonService {
    List<Cartoon> getAllCartoons();

    void addCartoon(CartoonForm cartoon);

    Cartoon getCartoon(long cartoonId);

    void deleteCartoon(Long cartoonId);

    void updateCartoon(Long cartoonId, CartoonForm cartoon);
}

