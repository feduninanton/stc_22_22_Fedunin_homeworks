package ru.inno.finalattestation.service;

import ru.inno.finalattestation.dto.FilmForm;
import ru.inno.finalattestation.models.Film;

import java.util.List;

public interface FilmService {
    void deleteFilm(Long filmId);

    List<Film> getAllFilms();

    void addFilm(FilmForm film);

    Film getFilm(long filmId);

    void updateFilm(Long filmId, FilmForm film);
}
