package ru.inno.finalattestation.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.finalattestation.dto.FilmForm;
import ru.inno.finalattestation.service.FilmService;


@Controller
@RequiredArgsConstructor

public class FilmController {

    private final FilmService filmService;

    @GetMapping("/films")
    public String GetFilmPage( @RequestParam(value = "orderBy", required = false) String orderBy,
                                 @RequestParam(value = "dir", required = false) String direction, Model model) {
        model.addAttribute("films", filmService.getAllFilms());
        return "films_page";
    }

    @PostMapping("/films")
    public String addFilm(FilmForm film) {
        filmService.addFilm(film);
        return "redirect:/films";
    }

    @GetMapping("/films/{film-id}")
    public String getFilmPage(@PathVariable("film-id")long filmId, Model model) {
        model.addAttribute("film", filmService.getFilm(filmId));
        return "film_page";
    }

    @GetMapping("/films/{film-id}/delete")
    public String deleteFilm(@PathVariable("film-id") Long filmId){
        filmService.deleteFilm(filmId);
        return "redirect:/films";
    }

    @PostMapping("/films/{film-id}/update")
    public String updateFilm(@PathVariable("film-id") Long filmId, FilmForm film) {
        filmService.updateFilm(filmId, film);
        return "redirect:/films";
    }

}
